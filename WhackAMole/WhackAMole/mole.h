#pragma once
// Library needed for using sprites, textures, and fonts
#include <SFML/Graphics.hpp>
// Library for handling collections of objects
#include <vector>

class mole
{
public: // access level (to be discussed later)
// Constructor
	mole(sf::Texture& playerTexture, sf::Vector2u screenSize);
	// Functions to call Player-specific code
	//void Input();
	//void Update(sf::Time frameTime);
	// Variables (data members) used by this class
	sf::Sprite sprite;
	//sf::Vector2f velocity;
	//float speed;
	int MoleLoc;
};

