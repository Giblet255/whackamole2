// Library Includes
#include <SFML/Graphics.hpp> // Library needed for creating and managing SFML windows
#include <SFML/Graphics.hpp> // this is the SFML package 
#include <SFML/Audio.hpp> // this load in the sound buffers
#include <string>
#include <vector>
#include "mole.h" // this load in the Item.h
#include "player.h"
#include <cstdlib> // we are using this to gen random Numbers
#include <time.h> // see above
 

// The main() Function - entry point for our program
int main()
{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// Set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode(669, 735), "Whack A Mole By Scott Gilbert", sf::Style::Titlebar | sf::Style::Close);
	
	// -----------------------------------------------
	// Game Setup
	// ----------------------------------------------

	//-------------------- Setting up Random Number
	srand(time(NULL)); // Clearing Number 

	//-------------- Sounds Game music
	sf::Music gameMusic;
	gameMusic.openFromFile("Assets/Audio/LOI.ogg");
	gameMusic.play();

	// Load the pickup sound effect file into a soundBuffer
	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/pickup.wav");
	// Setup a Sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound pickupSound;
	pickupSound.setBuffer(pickupSoundBuffer);

	// Load the victory sound effect file into a soundBuffer
	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");
	// Setup a Sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound victorySound;
	victorySound.setBuffer(victorySoundBuffer);
	
	// BackGound img ------------------------
	sf::Texture bgTexture;
	bgTexture.loadFromFile("Assets/Graphics/backgound.png");
	sf::Sprite BGImg;
	BGImg.setTexture(bgTexture);
	BGImg.setPosition(gameWindow.getSize().x / 2 - bgTexture.getSize().x / 2, 
	gameWindow.getSize().y / 2 - bgTexture.getSize().y / 2);

	// Setting Game Font and Txt ----------------------
	int Playerscore = 0;
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: " + std::to_string(Playerscore));
	scoreText.setCharacterSize(20);
	scoreText.setPosition(45, 30);

	

	// Timer Text 
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(20);
	timerText.setFillColor(sf::Color::White);
	timerText.setPosition(405,30);
	sf::Time timeLimit = sf::seconds(10.0f);
	sf::Time ADDtimeLimit = sf::seconds(2.0f);
	sf::Time timeRemaining = timeLimit;
	sf::Clock gameClock;

	// MoleTimer 
	sf::Text moletimerText;
	moletimerText.setFont(gameFont);
	moletimerText.setString("TempMole Timer: 0");
	moletimerText.setCharacterSize(20);
	moletimerText.setFillColor(sf::Color::White);
	moletimerText.setPosition(0,0);
	sf::Time moletimeLimit = sf::seconds(3.0f);
	sf::Time moletimeRemaining = moletimeLimit;
	sf::Clock molegameClock;

	
	// Setting up Player Sprite
	// Declare a texture variable called playerTexture
	sf::Texture playerTexture;
	// Load up our texture from a file path
	playerTexture.loadFromFile("Assets/Graphics/player2.png");
	// Declare a player object
	player playerObject(playerTexture, gameWindow.getSize());


	// Setting up Mole Sprite
	// Declare a texture variable called playerTexture
	sf::Texture moleTexture;
	// Load up our texture from a file path
	moleTexture.loadFromFile("Assets/Graphics/mole.png");
	// Declare a player object
	mole moleObject(moleTexture, gameWindow.getSize());


	// Game over variable to track if the game is done
	bool gameOver = false;

	// Game Over Text
	// Declare a text variable called gameOverText to hold our game over display
	sf::Text gameOverText;
	// Set the font our text should use
	gameOverText.setFont(gameFont);
	// Set the string of text that will be displayed by this text object
	gameOverText.setString("Game Over Please Press Space to play again");
	// Set the size of our text, in pixels
	gameOverText.setCharacterSize(20);
	// Set the colour of our text
	gameOverText.setFillColor(sf::Color::White);
	// Set the text style for the text
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	// Position our text in the top center of the screen
	gameOverText.setPosition(45, 709);


	



	//-----------------------------------------------
	// Game Loop
	//-----------------------------------------------
	// Repeat as long as the window is open
	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;
		// Loop through all events and poll them, putting each one into our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			// This section will be repeated for each event waiting to be processed
			// Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}
		}
		// End event polling loop
		// Player keybind input
		playerObject.Input();

		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();
		sf::Time moleframeTime = molegameClock.restart();

		// Update our time remaining based on how much time passed last frame
		timeRemaining = timeRemaining - frameTime;
		moletimeRemaining = moletimeRemaining - moleframeTime;

		// Check if time has run out
		if (timeRemaining.asSeconds() <= 0)
		{
			// Don't let the time go lower than 0
			timeRemaining = sf::seconds(0);
			// Perform these actions only once when the game first ends:
			if (gameOver == false)
			{
				// Set our gameOver to true now so we don't perform these actions again
				gameOver = true;
				// Stop the main music from playing
				gameMusic.stop();
				// Play our victory sound
				victorySound.play();
			}
		}

		// Only perform this update logic if the game is still running:
		if (!gameOver)
		{


			// Update our time display based on our time remaining
			timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));
			moletimerText.setString("TempMole Timer: " + std::to_string((int)moletimeRemaining.asSeconds()));


			// This Section move the mole for location to location 
			if (moletimeRemaining.asSeconds() <= 0)
			{
				moletimeRemaining = moletimeLimit;
				int MoleLoc2;



				MoleLoc2 = rand() % 9 + 1;
				//moleObject.MoleLoc = 1;

				if (MoleLoc2 == 1)
				{
					moleObject.sprite.setPosition(203, 221);
				}
				if (MoleLoc2 == 2)
				{
					moleObject.sprite.setPosition(303, 221);
				}
				if (MoleLoc2 == 3)
				{
					moleObject.sprite.setPosition(403, 221);
				}
				if (MoleLoc2 == 4)
				{
					moleObject.sprite.setPosition(203, 319);
				}if (MoleLoc2 == 5)
				{
					moleObject.sprite.setPosition(303, 319);
				}
				if (MoleLoc2 == 6)
				{
					moleObject.sprite.setPosition(403, 319);
				}
				if (MoleLoc2 == 7)
				{
					moleObject.sprite.setPosition(203, 420);
				}if (MoleLoc2 == 8)
				{
					moleObject.sprite.setPosition(303, 420);
				}
				if (MoleLoc2 == 9)
				{
					moleObject.sprite.setPosition(403, 420);
				}
			}



			// Move the player
			playerObject.Update(frameTime);

			

			// Setting hit box for the mole + player

			sf::FloatRect moleBounds = moleObject.sprite.getGlobalBounds();
			sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();
			if (moleBounds.intersects(playerBounds) && sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			{
				// Our player touched the item!
				// Add the item's value to the score
				// This Also Add Score and move the mole again 
				Playerscore = Playerscore + 100;
				scoreText.setString("Score: " + std::to_string(Playerscore));
				moleObject.MoleLoc = moleObject.MoleLoc + 2;
				timeRemaining = timeRemaining + ADDtimeLimit; 
				

				// Play the pickup sound
				pickupSound.play();

				if (moleObject.MoleLoc == 1)
				{
					moleObject.sprite.setPosition(203, 221);
				}
				if (moleObject.MoleLoc == 2)
				{
					moleObject.sprite.setPosition(303, 221);
				}
				if (moleObject.MoleLoc == 3)
				{
					moleObject.sprite.setPosition(403, 221);
				}
				if (moleObject.MoleLoc == 4)
				{
					moleObject.sprite.setPosition(203, 319);
				}if (moleObject.MoleLoc == 5)
				{
					moleObject.sprite.setPosition(303, 319);
				}
				if (moleObject.MoleLoc == 6)
				{
					moleObject.sprite.setPosition(403, 319);
				}
				if (moleObject.MoleLoc == 7)
				{
					moleObject.sprite.setPosition(203, 420);
				}if (moleObject.MoleLoc == 8)
				{
					moleObject.sprite.setPosition(303, 420);
				}
				if (moleObject.MoleLoc == 9)
				{
					moleObject.sprite.setPosition(403, 420);
				}
				// this reset the mole if the moleloc is too high
				if (moleObject.MoleLoc > 9)
				{
					moleObject.MoleLoc = 1;
				}


			}
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				timeRemaining = timeRemaining + ADDtimeLimit;
			}
			
			
			
			 

		}

		// Check if we should reset the game
		if (gameOver && sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			// Reset the game
			
			Playerscore = 0;
			scoreText.setString("Score: " + std::to_string(Playerscore));
			timeRemaining = timeLimit;
			moleObject.MoleLoc = 1;
			playerObject.sprite.setPosition(350,350);
			gameMusic.play();

			
			
			
			//gameMusic.play();
			gameOver = false;
		}



		// TODO: Update game state
		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------
		// Clear the window to a single colour
		gameWindow.clear(sf::Color(85,30,138));
		// Draw everything to the window

		// Drawing BackGound Image 
		gameWindow.draw(BGImg);
		
		// Draw all our items if Game running
		if (!gameOver)
		{

			// Drawing Score Board 
			gameWindow.draw(scoreText);
			gameWindow.draw(timerText);
			//gameWindow.draw(moletimerText);

			// Draw everything to the window
			gameWindow.draw(moleObject.sprite);
			gameWindow.draw(playerObject.sprite);
		}

		// Only draw these items if the game HAS ended:
		if (gameOver)
		{
			gameWindow.draw(gameOverText);
			gameWindow.draw(scoreText);
		}
		

		// TODO: Draw more stuff
		// Display the window contents on the screen
		gameWindow.display();

		// TODO: Draw graphics
		
	}
	// End of Game Loop
	return 0;
}
// End of main() FunctionFunction