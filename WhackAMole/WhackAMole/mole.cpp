#include "mole.h"
mole::mole(sf::Texture& moleTexture, sf::Vector2u screenSize)
{
	sprite.setTexture(moleTexture);
	//sprite.setPosition(	403,420	);

	//int MoleLoc;

	MoleLoc = rand() % 9 + 1;
	//MoleLoc = 2;
	
	// Working out pos for the moles 
	// 1 LOC 203,221
	// 2 LOC 303,221
	// 3 LOC 403,221
	// 4 LOC 203,319
	// 5 LOC 303,319
	// 6 LOC 403,319
	// 7 LOC 203,420
	// 8 LOC 303,420
	// 9 LOC 403,420

	if (MoleLoc == 1)
	{
		sprite.setPosition(203, 221);
	}
	if (MoleLoc == 2)
	{
		sprite.setPosition(303, 221);
	}
	if (MoleLoc == 3)
	{
		sprite.setPosition(403, 221);
	}
	
	if (MoleLoc == 4)
	{
		sprite.setPosition(203, 319);
	}if (MoleLoc == 5)
	{
		sprite.setPosition(303, 319);
	}
	if (MoleLoc == 6)
	{
		sprite.setPosition(403, 319);
	}
	if (MoleLoc == 7)
	{
		sprite.setPosition(203, 420);
	}if (MoleLoc == 8)
	{
		sprite.setPosition(303, 420);
	}
	if (MoleLoc == 9)
	{
		sprite.setPosition(403, 420);
	}
}