#include "player.h"

player::player(sf::Texture& playerTexture, sf::Vector2u screenSize)
{
	sprite.setTexture(playerTexture);
	sprite.setPosition(
		screenSize.x / 2 - playerTexture.getSize().x / 2,
		screenSize.y / 2 - playerTexture.getSize().y / 2
	);
	velocity.x = 0.0f;
	velocity.y = 0.0f;
	speed = 300.0f;

}
void player::Input()
{
	// Player keybind input
	// Start by zeroing out player velocity
	velocity.x = 0.0f;
	velocity.y = 0.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		// Move player up
		velocity.y = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		// Move player down
		velocity.y = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		// Move player right
		velocity.x = speed;
	}


}
void player::Update(sf::Time frameTime)
{
	// Move the player
	sprite.setPosition(sprite.getPosition() + velocity * frameTime.asSeconds());
}
